问你**Dubbo底层架构原理**是一样的，不求你说能看过**Spring Cloud的源码**，单单就是说搞明白他的一些底层架构原理，也是不错的

![Eureka服务注册中心的原理](../../docs/distributed-system/images/springCloud-study-theory.png)

-----

eurake 注册中心因为多级缓存的存在, 造成服务注册中心 在服务发现的时候 很慢

服务注册的过程

服务注册 ------> 服务注册列表-- 立马同步(定时检查) - -> readWriter缓存 ---(定时检查同步)---> readOnly缓存----(服务发现30s拉取)--> 本地服务注册注册列表 

两个比较重要的线程  都通过赋给 ScheduleExcutePool (); 执行定时任务.

```java
 /**
     * The task that fetches the registry information at specified intervals.
     *以指定的时间间隔获取注册表信息的任务。
     */
    class CacheRefreshThread implements Runnable {
        public void run() {
            refreshRegistry();
        }
    }

/**
     * The heartbeat task that renews the lease in the given intervals.
      * 在给定时间间隔内更新租约的心跳任务
     */
    private class HeartbeatThread implements Runnable {

        public void run() {
            if (renew()) {
                lastSuccessfulHeartbeatTimestamp = System.currentTimeMillis();
            }
        }
    }
```



```java
 InstanceInfoReplicator(DiscoveryClient discoveryClient, InstanceInfo instanceInfo, int replicationIntervalSeconds, int burstSize) {
        this.discoveryClient = discoveryClient;
        this.instanceInfo = instanceInfo;
        this.scheduler = Executors.newScheduledThreadPool(1,
                new ThreadFactoryBuilder()
                        .setNameFormat("DiscoveryClient-InstanceInfoReplicator-%d")
                        .setDaemon(true)
                        .build());

        this.scheduledPeriodicRef = new AtomicReference<Future>();

        this.started = new AtomicBoolean(false);
        this.rateLimiter = new RateLimiter(TimeUnit.MINUTES);
        this.replicationIntervalSeconds = replicationIntervalSeconds;
        this.burstSize = burstSize;

        this.allowedRatePerMinute = 60 * this.burstSize / this.replicationIntervalSeconds;
        logger.info("InstanceInfoReplicator onDemand update allowed rate per min is {}", allowedRatePerMinute);
    }

// InstanceInfoReplicator  run 方法
 public void run() {
        try {
            discoveryClient.refreshInstanceInfo();

            Long dirtyTimestamp = instanceInfo.isDirtyWithTime();
            if (dirtyTimestamp != null) {
                discoveryClient.register(); // 触发注册的地方
                instanceInfo.unsetIsDirty(dirtyTimestamp);
            }
        } catch (Throwable t) {
            logger.warn("There was a problem with the instance info replicator", t);
        } finally {
            Future next = scheduler.schedule(this, replicationIntervalSeconds, TimeUnit.SECONDS);
            scheduledPeriodicRef.set(next);
        }
    }




```

- 初始化定时任务  
	- 服务获取
	- 服务续约/心跳检测

```java
  private void initScheduledTasks() {
      
      // 服务获取
        if (clientConfig.shouldFetchRegistry()) {
            // registry cache refresh timer
            int registryFetchIntervalSeconds = clientConfig.getRegistryFetchIntervalSeconds();
            int expBackOffBound = clientConfig.getCacheRefreshExecutorExponentialBackOffBound();
            scheduler.schedule(
                    new TimedSupervisorTask(
                            "cacheRefresh",
                            scheduler,
                            cacheRefreshExecutor,
                            registryFetchIntervalSeconds,
                            TimeUnit.SECONDS,
                            expBackOffBound,
                            new CacheRefreshThread()
                    ),
                    registryFetchIntervalSeconds, TimeUnit.SECONDS);
        }

      //服务注册与续约
        if (clientConfig.shouldRegisterWithEureka()) {
            int renewalIntervalInSecs = instanceInfo.getLeaseInfo().getRenewalIntervalInSecs();
            int expBackOffBound = clientConfig.getHeartbeatExecutorExponentialBackOffBound();
            logger.info("Starting heartbeat executor: " + "renew interval is: " + renewalIntervalInSecs);

            // Heartbeat timer
            scheduler.schedule(
                    new TimedSupervisorTask(
                            "heartbeat",
                            scheduler,
                            heartbeatExecutor,
                            renewalIntervalInSecs,
                            TimeUnit.SECONDS,
                            expBackOffBound,
                            new HeartbeatThread()
                    ),
                    renewalIntervalInSecs, TimeUnit.SECONDS);

            // InstanceInfo replicator
            instanceInfoReplicator = new InstanceInfoReplicator(
                    this,
                    instanceInfo,
                    clientConfig.getInstanceInfoReplicationIntervalSeconds(),
                    2); // burstSize

            statusChangeListener = new ApplicationInfoManager.StatusChangeListener() {
                @Override
                public String getId() {
                    return "statusChangeListener";
                }

                @Override
                public void notify(StatusChangeEvent statusChangeEvent) {
                    if (InstanceStatus.DOWN == statusChangeEvent.getStatus() ||
                            InstanceStatus.DOWN == statusChangeEvent.getPreviousStatus()) {
                        // log at warn level if DOWN was involved
                        logger.warn("Saw local status change event {}", statusChangeEvent);
                    } else {
                        logger.info("Saw local status change event {}", statusChangeEvent);
                    }
                    instanceInfoReplicator.onDemandUpdate();
                }
            };

            if (clientConfig.shouldOnDemandUpdateStatusChange()) {
                applicationInfoManager.registerStatusChangeListener(statusChangeListener);
            }

            instanceInfoReplicator.start(clientConfig.getInitialInstanceInfoReplicationIntervalSeconds());
        } else {
            logger.info("Not registering with Eureka server per configuration");
        }
    }
```



- 服务注册与续约

```java
  /**
     * Register with the eureka service by making the appropriate REST call.
       服务注册 采用rest 请求的方式 讲instanceInfo 传递进去  要么200 要么404 或者直接报错
     */
    boolean register() throws Throwable {
        logger.info(PREFIX + appPathIdentifier + ": registering service...");
        EurekaHttpResponse<Void> httpResponse;
        try {
            httpResponse = eurekaTransport.registrationClient.register(instanceInfo);
        } catch (Exception e) {
            logger.warn("{} - registration failed {}", PREFIX + appPathIdentifier, e.getMessage(), e);
            throw e;
        }
        if (logger.isInfoEnabled()) {
            logger.info("{} - registration status: {}", PREFIX + appPathIdentifier, httpResponse.getStatusCode());
        }
        return httpResponse.getStatusCode() == 204;
    }

    /**
     * Renew with the eureka service by making the appropriate REST call
       服务续约也是通过 rest的方式 要么200 要么404 或者直接报错
     */
    boolean renew() {
        EurekaHttpResponse<InstanceInfo> httpResponse;
        try {
            httpResponse = eurekaTransport.registrationClient.sendHeartBeat(instanceInfo.getAppName(), instanceInfo.getId(), instanceInfo, null);
            logger.debug("{} - Heartbeat status: {}", PREFIX + appPathIdentifier, httpResponse.getStatusCode());
            if (httpResponse.getStatusCode() == 404) {
                REREGISTER_COUNTER.increment();
                logger.info("{} - Re-registering apps/{}", PREFIX + appPathIdentifier, instanceInfo.getAppName());
                return register();
            }
            return httpResponse.getStatusCode() == 200;
        } catch (Throwable e) {
            logger.error("{} - was unable to send heartbeat!", PREFIX + appPathIdentifier, e);
            return false;
        }
    }
```



----

**Eureka、Ribbon、Feign、Zuul**

就是优化并发冲突

如果你基于**Spring Cloud**对外发布一个接口，实际上就是支持**http协议**的，对外发布的就是一个最最普通的**Spring MVC的http接口**

**feign**，他是对一个接口打了一个注解，他一定会针对这个注解标注的接口生成动态代理，然后你针对feign的动态代理去调用他的方法的时候，此时会在底层生成http协议格式的请求，/order/create?productId=1

底层的话，使用HTTP通信的框架组件，**HttpClient**，**先得使用Ribbon去从本地的Eureka注册表的缓存里获取出来对方机器的列表，然后进行负载均衡，选择一台机器出来，接着针对那台机器发送Http请求过去即可**

配置一下不同的请求路径和服务的对应关系，你的请求到了网关，他直接查找到匹配的服务，然后就直接把请求转发给那个服务的某台机器，**Ribbon从Eureka本地的缓存列表里获取一台机器，负载均衡，把请求直接用HTTP通信框架发送到指定机器上去**



**我们的课程每天都会有一个作业，引导大家把学习到的项目经验、技术方案和生产优化落地到自己负责的项目中去，让大家出去面试的时候，可以把各种技术结合自己的项目来回答面试官的各种深度拷问**

**大家不要小看这个，根据我多年的面试经验来看，拥有这个技能的人凤毛麟角，这种人出去绝对是各大公司争抢的对象。**

**我们的课程每天都会有一个作业，引导大家把学习到的项目经验、技术方案和生产优化落地到自己负责的项目中去，让大家出去面试的时候，可以把各种技术结合自己的项目来回答面试官的各种深度拷问**

**大家不要小看这个，根据我多年的面试经验来看，拥有这个技能的人凤毛麟角，这种人出去绝对是各大公司争抢的对象。**

**所以希望大家好好完成每天的作业，我布置的大量作业，就是为了帮你锻造出这种能力**

**学习课程以及完成作业的过程中，大家一定会有很多的问题，可以到专栏的评论区去提问**

**每天我都会和之前带出来的一批阿里、蚂蚁金服、滴滴的优秀同学给大家进行答疑，并且我们还有专门的付费用户的微信群，大家可以在微信群里跟我们一起进行技术交流**

**如果你能坚持下来，学满6季，还可以获取私人定制的面试一条龙VIP服务**

**如果是连续6季面试训练营都购买的同学，还可以获取面试一条龙VIP服务**

**具体信息大家看“儒猿技术窝”公众号的知识店铺内的训练营详情即可**

**具体可参见训练营目录下的《训练营专属服务》文档。简单来说，这个私人定制的面试VIP服务，会为你的跳槽面试全程保驾护航**

**“儒猿技术窝”**，找到我们的训练营的详情页面

